
public class Client {
	private String nom;
	private Serveur S;
	/**
	 * 
	 * constructeur du client
	 */
	public Client(String nom)
	{
		this.nom = nom;
        this.S = null;

	}
	
	public boolean seConnecter(Serveur S)
	{
		boolean reponse = S.Connecter(this);
        if(reponse == true){
            this.S = S;
            return true;
        }
        else 
            return false;
	}
	
	public void envoyer(String message)
	{
		  if(S != null)
	            S.Diffuser(nom + ":" + message);
	}
	
	public String recevoir(String message)
	{
		System.out.print (message + " pour nom : " + nom + " \n");	
		
        return message;
	}

}
