import java.util.ArrayList;
import java.util.Iterator;

public class Serveur {

    private ArrayList<Client> Clients;

    /**
     * Constructeur Serveur
     */
	public Serveur()
	{
        Clients = new ArrayList<Client>();

	}
	
	public boolean Connecter(Client c)
	{
        if(Clients.contains(c)) 
        {
            return false;
        }
        else 
        {
            return Clients.add(c);
        }
	}
	
	public void Diffuser(String message)
	{

		
		Iterator<Client> liste_clients = Clients.iterator();
        
        while(liste_clients.hasNext()) {
            liste_clients.next().recevoir(message);
        }
	}
}
